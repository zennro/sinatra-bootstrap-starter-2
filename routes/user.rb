#routes

### Sign up ###
get '/signup' do
  haml :signup
end

post '/signup' do
  user = User.create(params[:user])
  user.password_salt = BCrypt::Engine.generate_salt
  user.password_hash = BCrypt::Engine.hash_secret(params[:user][:password], user.password_salt)
  if user.save
  	flash[:info] = "Thank you for registering #{user.email}"
  	session[:user] = user.token
  	redirect "/"
  else
    flash[:error] = "Doh! Something went wrong!"
  	redirect "/signup"
  end
end

### Log in ###
get '/login' do
  if current_user
    redirect_last
    
  else
    haml :login
  end
end

post '/login' do
  user = User.first(:email => params[:email])
  if user
  	if user.password_hash == BCrypt::Engine.hash_secret(params[:password], user.password_salt)
  	  session[:user] = user.token
      if params[:remember_me]
  	    response.set_cookie "user", {:value => user.token, :expires => (Time.now + 52*7*24*60*60)}
  	  end
      flash[:info] = "Successfully logged in"
  	  redirect_last
      
    else
      flash[:error] = "Email/Password combination does not match"
      redirect "/login"
    end
  else
  	flash[:error] = "That email address is not recognised"
  	redirect "/login"
  end
end

### Log out ###

get '/logout' do
  current_user.generate_token
  response.delete_cookie "user"
  session[:user] = nil
  flash[:info] = "Successfully logged out"
  redirect "/login"
end

### User Profile Page ###
get '/:username' do
  haml :profile
end

### Edit user account ###
get '/:username/edit' do
  @user = User.first()
  haml :edit
end

put '/:username/edit' do
  user = User.first
  success = user.update!(params[:user])
  
  if success
    flash[:info] = "Successfully updated!"
    redirect_last
  else
    flash[:error] = "Doh! Something went wrong!"
    redirect "/:username/edit"
  end
end
